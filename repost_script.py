# encoding: utf8
import logging
import os
import re
from datetime import datetime
from io import open
from time import sleep

log_path = '/var/log/clicker/'
log_files = ['success', 'error']
for log_file in log_files:
    filename = log_path + log_file + '.log'
    if os.path.isfile(filename):
        os.remove(filename)
logging.basicConfig(filename=log_path + 'logging.log')


def write_log(message, success=False, path=log_path):
    if not os.path.exists(path):
        os.mkdir(path)
    filename = path + ('success' if success else 'error') + '.log'
    with open(filename, 'a') as f:
        f.write(unicode(message + "\n"))


def get_data_from_file(filename, stripparts=True, sep='\n'):
    """
    :param filename: str
    :param stripparts: bool
    :param sep: str
    :return: list of str
    """
    lines = []
    path = '/var/www/repost_script_server/sikulix/'
    try:
        with open(path + filename, encoding='utf8') as f:
            file_content = f.read()
            if file_content.endswith(sep.strip()):
                file_content = file_content[: file_content.rfind(sep.strip())]
            for part in file_content.split(sep):
                if stripparts:
                    part = part.strip()
                if not part:
                    continue
                lines.append(part)
    except IOError:
        write_log(u'Нету файла: ' + filename)
        close()
    return lines[0]


def close(success=False):
    if success:
        text = u'Репост успешно завершен'
        write_log(text, success=success)
    exit(int(not success))


def close_chrome():
    type(Key.F6)
    sleep(1)
    type('w', Key.CTRL)
    sleep(1)
    type(Key.F4, Key.ALT)


def turn_stylish_off():
    fancy_click(Pattern("1522311474517.png").similar(0.85))
    sleep(1)
    fancy_click("1522054183622.png")
    sleep(1)
    type(Key.ESC)


def fancy_click(image):
    try:
        click(image)
    except FindFailed:
        write_log(u'FindFailed at click')
        logging.exception("FindFailed at click")
        close_chrome()
        exit(1)


def fancy_paste(image, text):
    try:
        paste(image, text)
    except FindFailed:
        write_log(u'FindFailed at paste')
        logging.exception('FindFailed at paste')
        close_chrome()
        exit(1)

def fancy_wait(image, time):
    try:
        wait(image, time)
    except FindFailed:
        write_log(u'FindFailed at wait')
        logging.exception('FindFailed at wait')
        close_chrome()
        exit(1)


def fancy_find(image, region=SCREEN):
    try:
        return region.find(image)
    except FindFailed:
        write_log(u'FindFailed at find')
        logging.exception('FindFailed at find')
        close_chrome()
        exit(1)


def scrol_down(image, region=SCREEN):
    if not region.exists(image):
        mouseMove(Pattern("1525705310528.png").similar(0.80))
    i = 0
    while not region.exists(image):
        mouseDown(Button.LEFT)
        sleep(1)
        mouseUp(Button.LEFT)
        i += 1
        if i > 25:
            break


def scrol_with_mouse():
    wheel("1527912793015.png", Button.WHEEL_DOWN, 2)


def fix_ext(image):
    type('t', Key.CTRL)
    sleep(1)
    paste('chrome://extensions')
    sleep(1)
    type(Key.ENTER)
    sleep(1.5)
    fancy_wait(image, 10)
    fancy_click(image)
    sleep(1)
    type('w', Key.CTRL)


files = ['link', 'public', 'text', 'time', 'person']
link = get_data_from_file(files[0] + '.txt')
public = get_data_from_file(files[1] + '.txt')
post_text = get_data_from_file(files[2] + '.txt',
                               stripparts=False, sep='\n_end_\n')
time = get_data_from_file(files[3] + '.txt')
person = get_data_from_file(files[4] + '.txt')


regex = re.compile('(wall-|wall)\d+_\d+$')
match = regex.search(link)
if not match:
    write_log(u'Неверная ссылка на пост: "' + link + '"')
    close_chrome()
    close()
link = 'https://vk.com/' + match.group(0)

regex = re.compile('^\d{1,2}\.\d{1,2}\.\d{4} \d{1,2}:\d{2}$')
match = regex.search(time)
if not match:
    write_log(u'Неверный формат времени: "' + time + '"')
    write_log(u'Время должно быть в формате: "ДД.ММ.ГГГГ ЧЧ:ММ" или'
              u'"Д.М.ГГГГ Ч:ММ" или любым между этими форматами')
    close_chrome()
    close()


text_time = time
date, time = text_time.split(' ')
day, month, year = date.split('.')
hour, minute = time.split(':')
time = {
        'year': int(year),
        'month': int(month),
        'day': int(day),
        'hour': int(hour),
        'minute': minute
        }

now = datetime.now()
try:
    time_obj = datetime(time['year'], time['month'],time['day'],
                        time['hour'], int(time['minute']))
except ValueError:
    write_log(u'Неверное время для поста,'
              u'скорее всего такого дня в этом месяце нет')
    close_chrome()
    close()
# noinspection PyUnboundLocalVariable
if time_obj < now:
    write_log(u'Невозможно отложить пост в прошлое. Это не машина времени!')
    close_chrome()
    close()
time.update({
            'month_dif': time['month'] - now.month + 12 * (time['year'] - now.year)
            })

post = {
    'url': link,
    'public': public,
    'text': post_text,
    'time': time,
    'person': person
}


fancy_wait("1522308231375.png", 15)
sleep(1)
if not (exists(Pattern("1532850423034.png").similar(0.80)) or
        exists(Pattern("1532850462960.png").similar(0.85))):
    fix_ext(Pattern("1532850546912.png").targetOffset(141,55))
    print 'fixed Stylish'

if not exists(Pattern("1527791066295.png").similar(0.86)):
    fix_ext(Pattern("1527790896200.png").targetOffset(144,56))
    print 'fixed TamperMonkey'
type('s', Key.CTRL + Key.SHIFT)
sleep(6)
if not exists(Pattern("1525881124500.png").similar(0.85)):
    fix_ext(Pattern("1527499635812.png").similar(0.85).targetOffset(140,59))
    type('s', Key.CTRL + Key.SHIFT)
    print 'fixed SessionBox'
fancy_wait(Pattern("1525881124500.png").similar(0.85), 10)
sleep(1)
fancy_paste(Pattern("1525881203532.png").similar(0.90).targetOffset(-66,9), post['person'])
sleep(1)
fancy_click(Pattern("1525881316184.png").similar(0.94).targetOffset(-149,46))
sleep(7.5)
fancy_wait("1526579066888.png", 25)
print 'opened VK'
sleep(2.5)

stylish = True
type(Key.F6)
sleep(1)
paste(post['url'])
sleep(1)
type(Key.ENTER)
sleep(5)
type(Key.ESC)
fancy_wait("1526579066888.png", 30)
print 'opened VK post'
sleep(7.5)
top_bar = Region(getLastMatch())
repost_region = Region(top_bar.getX(),
                       top_bar.getY() + top_bar.getH(),
                       SCREEN.getW() - top_bar.getX(),
                       SCREEN.getH() - (top_bar.getY() + top_bar.getH())
                       )
fancy_wait(Pattern("1522483140294.png").similar(0.85), 40)
sleep(1)
repost_pattern = Pattern("repost_pattern.png").similar(0.80).targetOffset(4,1)
i = 0
if not repost_region.exists(repost_pattern):
    while not repost_region.exists(repost_pattern):
        for i in range(2):
            type(Key.DOWN)
        sleep(1)
        i += 1
        if i > 15:
            break
fancy_click(fancy_find(repost_pattern, repost_region))
print 'clicker on repost icon'
sleep(2.5)
fancy_wait(Pattern("1558375799717.png").similar(0.60), 30)
print 'repost dialog is opened'
sleep(1)
fancy_click("1549028227085.png")
sleep(3)
paste(post['public'])
sleep(3)
type(Key.ENTER)
sleep(1)
print 'public is done'
fancy_click(Pattern("1521833112864.png").targetOffset(-62,0))
print 'clicked on image button'
sleep(2)
fancy_wait(Pattern("1521872439294.png").similar(0.80).targetOffset(-96,26), 25)
print 'load image dialog is opened'
sleep(2.5)
fancy_click(Pattern("1521872439294.png").similar(0.80).targetOffset(-96,26))
print 'clicked on load image button'
sleep(2)
fancy_wait(Pattern("1525704724822.png").targetOffset(42,1), 25)
print 'open button is loaded'
sleep(1)
fancy_click(Pattern("1525704724822.png").targetOffset(42,1))
print 'click on open button'
sleep(2)
fancy_wait("1522135869388.png", 25)
print 'repost dialog is ready'
sleep(1)
fancy_paste(Pattern("1549029306028.png").targetOffset(-186,6), post['text'])
print 'pasted text'
sleep(1)
fancy_click(Pattern("1526244236941.png").similar(0.80).targetOffset(-194,-2))
print 'clicked on open calendar button'
fancy_wait(Pattern("1521834573331.png").similar(0.80), 3)
print 'calendar is opened'
fancy_click("1521834628119.png")
print 'clicked on months image'
months = [Pattern("1522487795111.png").similar(0.91), Pattern("1522487815279.png").similar(0.91),
          Pattern("1551385499125.png").similar(0.94), Pattern("1522487860818.png").similar(0.90),
          Pattern("1522487874295.png").similar(0.91), Pattern("1522487890832.png").similar(0.91),
          Pattern("1522487908669.png").similar(0.90), Pattern("1522488069752.png").similar(0.90),
          Pattern("1522488084174.png").similar(0.90), Pattern("1522488097213.png").similar(0.92),
          Pattern("1522488110506.png").similar(0.92), Pattern("1522488122691.png").similar(0.91)]
fancy_wait(Pattern("1550760328157.png").similar(0.85), 25)
print 'month picker is ready'
if (not exists(months[now.month-1])) and (exists(months[now.month-1+1])):
    post['time']['month_dif'] -= 1
i = 0
if i < post['time']['month_dif']:
    while i < post['time']['month_dif']:
        fancy_click(Pattern("1550761892205.png").similar(0.88).targetOffset(2,1))
        sleep(1)
        i += 1
        if i > 20:
            break
elif i > post['time']['month_dif']:
    while i > post['time']['month_dif']:
        fancy_click(Pattern("1550761859452.png").similar(0.85).targetOffset(-1,1))
        sleep(1)
        i -= 1
        if i < -20:
            break
print 'Choosed month'
days = [Pattern("1526486523426.png").similar(0.85), Pattern("1526486532350.png").similar(0.85),
        Pattern("1551552653630.png").similar(0.91), Pattern("1526486549385.png").similar(0.85),
        Pattern("1526486556308.png").similar(0.89), Pattern("1526486562801.png").similar(0.89),
        Pattern("1526486569236.png").similar(0.90), Pattern("1526486576389.png").similar(0.85),
        Pattern("1526486583654.png").similar(0.85), Pattern("1528399003718.png").similar(0.90),
        Pattern("1526486284952.png").similar(0.85), Pattern("1526486291861.png").similar(0.88),
        Pattern("1526486302219.png").similar(0.92), Pattern("1526486320262.png").similar(0.88),
        Pattern("1526486328146.png").similar(0.95), Pattern("1526486337392.png").similar(0.93),
        Pattern("1528399057835.png").similar(0.90), Pattern("1526486350626.png").similar(0.95),
        Pattern("1526486357508.png").similar(0.91), Pattern("1526486370755.png").similar(0.91),
        Pattern("1526486378310.png").similar(0.92), Pattern("1526486384949.png").similar(0.92),
        Pattern("1526486392244.png").similar(0.91), Pattern("1526486399156.png").similar(0.86),
        Pattern("1551203495153.png").similar(0.93), Pattern("1551203275756.png").similar(0.92),
        Pattern("1526486439253.png").similar(0.94), Pattern("1526486447359.png").similar(0.86),
        Pattern("1526486454333.png").similar(0.95), Pattern("1526486460958.png").similar(0.89),
        Pattern("1526486467916.png").similar(0.93)]

sleep(1.5)
fancy_click(days[post['time']['day'] - 1])
sleep(1.5)
print 'Choosed day'
fancy_wait(Pattern("1549030510525.png").similar(0.80), 10)
print 'time picker is ready'
fancy_click(Pattern("1549030556665.png").similar(0.90).targetOffset(-8,2))
print 'clicked on hour picker'
sleep(1.5)
type(str(post['time']['hour']))
sleep(1.5)
type(Key.ENTER)
sleep(1.5)
print 'Choosed hour'
fancy_click(Pattern("1549030609581.png").targetOffset(-50,15))
print 'clicked on minute picker'
sleep(1.5)
type(post['time']['minute'])
sleep(1.5)
type(Key.ENTER)
sleep(1.5)
print 'Choosed minute'
fancy_click(Pattern("1526244760071.png").similar(0.80).targetOffset(129,0))
print 'Clicked on repost button'
sleep(5)
if exists(Pattern("no_access_error.png").similar(0.80)):
    write_log('Sessionbox error')
    close_chrome()
    close()
if exists(Pattern("1526206889365.png").similar(0.80)):
    write_log(u'На это время уже запланирован пост')
    close_chrome()
    close()
if exists(Pattern("1528385891629.png").similar(0.80)):
    write_log(u'Максимальное количество отложенных публикаций '
              u'на один день - 25. Это ограничение ВК')
    close_chrome()
    close()
if not exists("1526579066888.png"):
    write_log('Unknown error')
    close_chrome()
    close()
close_chrome()
close(True)
